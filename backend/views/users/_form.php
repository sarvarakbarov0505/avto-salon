<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\User;
/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */
$dirname = preg_replace("/[^a-zA-Z0-9\s]/", '',$model->tableName())
?>

<div class="user-form">

    <?php $form = ActiveForm::begin([
        'options' => [
            'enctype' => 'multipart/form-data',
            'id' => 'user-form'
            ]]); ?>
    <div class="row">
        <div class="col-md-4">
            <?php
                $input = '<label for="avatar" title="Выберите" data-toggle="tooltip" id="image_label" onmousemove="style.cursor=\'pointer\'">'. $model->getImageAdress().'</label>
                <input type="file" name="" id="avatar" style="display: none;" accept="image/*">';
                $template = '<div class="row">
                                <div class="col-md-9">
                                    {input}'.$input.'{error}
                                    <p class="text-center">{label}</p>
                                </div>
                            </div>';
            ?>
            <?= $form->field($model, 'avatar',['template' => $template])->textInput(['id' => 'temp_address','value' => $model->avatar]) ?>
        </div>
        <div class="col-md-6">
            <div class="col-md-6">
                <?= $form->field($model, 'fio')->textInput() ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'role')->dropDownLIst(User::getRoleLIst()) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'username')->textInput() ?>
            </div>
            <div class="col-md-6">
                <?php
                    if($model->isNewRecord) :
                ?>
                    <?= $form->field($model, 'password')->textInput() ?>
                <?php else: ?>
                    <?= $form->field($model, 'new_password')->textInput() ?>
                <?php endif; ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'status')->dropDownLIst(User::getStatusList()) ?>
            </div>
        </div>
    </div>

    <di class="form-group">
        <?= Html::submitButton(Yii::t('backend/app', 'Save'), ['class' => 'btn btn-success']) ?>
    </>
    <?php ActiveForm::end(); ?>

</div>

<?php
$this->registerJS(<<<JS
    $("#avatar").on('change',function(e){
        var file = $( '#avatar' )[0].files[0];
        var data = new FormData();

        var d = new Date();
        var new_name = d.getFullYear() + '-' + d.getMonth() + '-' + d.getDate() + '_' +d.getHours() + '-' + d.getMinutes() + '-' + d.getSeconds();
        var filename = file.name;
        name = filename.split('.').shift();
        var ext = filename.split('.').pop();
        new_name = name + '(' + new_name + ")." + ext;
        data.append('file[]', file) ;
        data.append('dir_name', 'users') ;
        data.append('names[]', new_name);
        data.append('old_image', $("#temp_address").val());
        $("#temp_address").val(new_name);
        
        $.ajax({
            url: '/adminpanel/ru/users/upload-image',
            type: 'POST',
            data: data,
            processData: false,
            contentType: false,
            success: function(success){
                console.log(success);
                
                $("#image_label img").attr('style','width:70px;height:70px');
                $("#image_label img").attr('src',success);
            },
            error: function(xhr, textStatus, error){
                console.log(xhr.statusText);
                console.log(textStatus);
                console.log(error);
                alert("Error occur uploading image. Try again )");
                $("#image_label img").attr('src','../../uploads/noimg.png');
            },
            //Do not cache the page
            cache: false,

            //@TODO start here
            xhr: function() {  // custom xhr
                myXhr = $.ajaxSettings.xhr();
                if (myXhr.upload) {
                    $("#image_label img").attr('src','../../uploads/zz.gif');
                    return myXhr;
                }
            }
        });
    });


JS
)

?>