<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\User;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = $model->fio;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend/app', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('backend/app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('backend/app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('backend/app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'fio',
            'username',
            [
                'attribute' => 'status',
                'value' => function($model){
                    return User::getStatusList()[$model->status];
                }
            ],
            [
                'attribute' => 'role',
                'value' => function($model){
                    return User::getRoleList()[$model->role];
                }
            ],
            [
                'attribute' => 'created_at',
                'value' => function($model){
                    return $model->getCreatAt();
                }
            ],
            [
                'attribute' => 'updated_at',
                'value' => function($model){
                    return $model->getUpdatedAt();
                }
            ],
        ],
    ]) ?>

</div>
