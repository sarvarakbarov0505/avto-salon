<?php 
use common\models\User;
return [
    [
        'class' => '\yii\grid\DataColumn',
        'attribute' => 'fio',
    ],
    [
        'class' => 'yii\grid\DataColumn',
        'attribute' => 'role',
        'content' => function($model){
            return $model->getRoleList()[$model->role];
        },
        'filter' => User::getRoleList()
    ],
    [
        'class' => 'yii\grid\DataColumn',
        'attribute' => 'status',
        'content' => function($model){
            return $model->getStatusList()[$model->status];
        },
        'filter' => User::getStatusList()
    ],
    [
        'class' => '\yii\grid\ActionColumn',
    ]
];
