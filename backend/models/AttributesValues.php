<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "attributes_values".
 *
 * @property int $id
 * @property int|null $attribute_id
 * @property string|null $name
 *
 * @property Attributes $attribute0
 */
class AttributesValues extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'attributes_values';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['attribute_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['attribute_id'], 'exist', 'skipOnError' => true, 'targetClass' => Attributes::className(), 'targetAttribute' => ['attribute_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend/attribute-values', 'ID'),
            'attribute_id' => Yii::t('backend/attribute-values', 'Attribute ID'),
            'name' => Yii::t('backend/attribute-values', 'Name'),
        ];
    }

    /**
     * Gets query for [[Attribute0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAttribute0()
    {
        return $this->hasOne(Attributes::className(), ['id' => 'attribute_id']);
    }
}
