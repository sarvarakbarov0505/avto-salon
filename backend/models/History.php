<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "user_actions_history".
 *
 * @property int $id
 * @property int|null $user_id
 * @property string|null $model_name
 * @property int|null $model_id
 * @property int|null $craeted_at
 * @property int|null $updated_at
 *
 * @property User $user
 */
class History extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_actions_history';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'model_id', 'craeted_at', 'updated_at'], 'integer'],
            [['model_name'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend/history', 'ID'),
            'user_id' => Yii::t('backend/history', 'User ID'),
            'model_name' => Yii::t('backend/history', 'Model Name'),
            'model_id' => Yii::t('backend/history', 'Model ID'),
            'craeted_at' => Yii::t('backend/history', 'Craeted At'),
            'updated_at' => Yii::t('backend/history', 'Updated At'),
        ];
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery|UserQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * {@inheritdoc}
     * @return HistoryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new HistoryQuery(get_called_class());
    }
}
