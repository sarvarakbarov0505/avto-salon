<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "categories".
 *
 * @property int $id
 * @property string|null $name
 * @property int|null $enabled
 * @property string|null $img
 *
 * @property Autos[] $autos
 * @property CategoriesAttributes[] $categoriesAttributes
 * @property Attributes[] $attributes0
 */
class Categories extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'categories';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['enabled'], 'integer'],
            [['name', 'img'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend/categories', 'ID'),
            'name' => Yii::t('backend/categories', 'Name'),
            'enabled' => Yii::t('backend/categories', 'Enabled'),
            'img' => Yii::t('backend/categories', 'Img'),
        ];
    }

    /**
     * Gets query for [[Autos]].
     *
     * @return \yii\db\ActiveQuery|AutosQuery
     */
    public function getAutos()
    {
        return $this->hasMany(Autos::className(), ['category_id' => 'id']);
    }

    /**
     * Gets query for [[CategoriesAttributes]].
     *
     * @return \yii\db\ActiveQuery|CategoriesAttributesQuery
     */
    public function getCategoriesAttributes()
    {
        return $this->hasMany(CategoriesAttributes::className(), ['categories_id' => 'id']);
    }

    /**
     * Gets query for [[Attributes0]].
     *
     * @return \yii\db\ActiveQuery|AttributesQuery
     */
    public function getAttributes0()
    {
        return $this->hasMany(Attributes::className(), ['id' => 'attributes_id'])->viaTable('categories_attributes', ['categories_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return CategoriesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CategoriesQuery(get_called_class());
    }
}
