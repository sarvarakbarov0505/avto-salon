<?php

namespace backend\models;

use yii\base\Model;
use yii\web\UploadedFile;

class UploadForm extends Model
{
    /**
     * @var UploadedFile[]
     */
    public $imageFiles;
    public $imageFile;

    public function rules()
    {
        return [
            [['imageFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg'],
            [['imageFiles'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg', 'maxFiles' => 4],
        ];
    }

    public function UploadImage($dir)
    {
        $source_path = '/web/uploads/trash/';
        $destination_path = '/web/uploads/'.$dir.'/';

        if($this->imageFile != "")
        {
            $value = $this->imageFile;
            if ($res != -1) {
                $ext = substr(strrchr($value, "."), 1);
                $fileName = $this->id . '-' . Yii::$app->security->generateRandomString() . '.' . $ext;
                return rename($source_path.$value, $destination_path.$fileName);
            }
        }
    }

    //uchirish
    public static function deleteImage($dir,$img)
    {
        $path = '/web/uploads/'.$dir.'/';
        if(file_exists($path.$img)){
            unlink($path.$img);
        }
    }

    //
    public static function getImageAddress($dir,$img)
    {
        $dir = self::DIR_NAME;
        if ($image)
            return "/web/uploads/".$dir."/$image";
        return "/web/uploads/noimg.jpg";
    }

}
