<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "images".
 *
 * @property int $id
 * @property int|null $auto_id
 * @property string|null $original_image
 * @property string|null $image
 *
 * @property Autos $auto
 */
class Images extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'images';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['auto_id'], 'integer'],
            [['original_image', 'image'], 'string', 'max' => 255],
            [['auto_id'], 'exist', 'skipOnError' => true, 'targetClass' => Autos::className(), 'targetAttribute' => ['auto_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend/images', 'ID'),
            'auto_id' => Yii::t('backend/images', 'Auto ID'),
            'original_image' => Yii::t('backend/images', 'Original Image'),
            'image' => Yii::t('backend/images', 'Image'),
        ];
    }

    /**
     * Gets query for [[Auto]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAuto()
    {
        return $this->hasOne(Autos::className(), ['id' => 'auto_id']);
    }
}
