<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "attributes".
 *
 * @property int $id
 * @property string|null $name
 * @property int|null $required
 * @property int|null $input_type
 *
 * @property AttributesValues[] $attributesValues
 * @property AutosAttributes[] $autosAttributes
 * @property Autos[] $autos
 * @property CategoriesAttributes[] $categoriesAttributes
 * @property Categories[] $categories
 */
class Attributes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'attributes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['required', 'input_type'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend/attributes', 'ID'),
            'name' => Yii::t('backend/attributes', 'Name'),
            'required' => Yii::t('backend/attributes', 'Required'),
            'input_type' => Yii::t('backend/attributes', 'Input Type'),
        ];
    }

    /**
     * Gets query for [[AttributesValues]].
     *
     * @return \yii\db\ActiveQuery|AttributesValuesQuery
     */
    public function getAttributesValues()
    {
        return $this->hasMany(AttributesValues::className(), ['attribute_id' => 'id']);
    }

    /**
     * Gets query for [[AutosAttributes]].
     *
     * @return \yii\db\ActiveQuery|AutosAttributesQuery
     */
    public function getAutosAttributes()
    {
        return $this->hasMany(AutosAttributes::className(), ['attributes_id' => 'id']);
    }

    /**
     * Gets query for [[Autos]].
     *
     * @return \yii\db\ActiveQuery|AutosQuery
     */
    public function getAutos()
    {
        return $this->hasMany(Autos::className(), ['id' => 'autos_id'])->viaTable('autos_attributes', ['attributes_id' => 'id']);
    }

    /**
     * Gets query for [[CategoriesAttributes]].
     *
     * @return \yii\db\ActiveQuery|CategoriesAttributesQuery
     */
    public function getCategoriesAttributes()
    {
        return $this->hasMany(CategoriesAttributes::className(), ['attributes_id' => 'id']);
    }

    /**
     * Gets query for [[Categories]].
     *
     * @return \yii\db\ActiveQuery|CategoriesQuery
     */
    public function getCategories()
    {
        return $this->hasMany(Categories::className(), ['id' => 'categories_id'])->viaTable('categories_attributes', ['attributes_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return AttributesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new AttributesQuery(get_called_class());
    }
}
