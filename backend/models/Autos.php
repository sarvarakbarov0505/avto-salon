<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "autos".
 *
 * @property int $id
 * @property string|null $name
 * @property int|null $brand_id
 * @property string|null $slug
 * @property int|null $category_id
 *
 * @property Brands $brand
 * @property Categories $category
 * @property AutosAttributes[] $autosAttributes
 * @property Attributes[] $attributes0
 * @property Images[] $images
 */
class Autos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'autos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['brand_id', 'category_id'], 'integer'],
            [['name', 'slug'], 'string', 'max' => 255],
            [['brand_id'], 'exist', 'skipOnError' => true, 'targetClass' => Brands::className(), 'targetAttribute' => ['brand_id' => 'id']],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Categories::className(), 'targetAttribute' => ['category_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend/autos', 'ID'),
            'name' => Yii::t('backend/autos', 'Name'),
            'brand_id' => Yii::t('backend/autos', 'Brand ID'),
            'slug' => Yii::t('backend/autos', 'Slug'),
            'category_id' => Yii::t('backend/autos', 'Category ID'),
        ];
    }

    /**
     * Gets query for [[Brand]].
     *
     * @return \yii\db\ActiveQuery|BrandsQuery
     */
    public function getBrand()
    {
        return $this->hasOne(Brands::className(), ['id' => 'brand_id']);
    }

    /**
     * Gets query for [[Category]].
     *
     * @return \yii\db\ActiveQuery|CategoriesQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Categories::className(), ['id' => 'category_id']);
    }

    /**
     * Gets query for [[AutosAttributes]].
     *
     * @return \yii\db\ActiveQuery|AutosAttributesQuery
     */
    public function getAutosAttributes()
    {
        return $this->hasMany(AutosAttributes::className(), ['autos_id' => 'id']);
    }

    /**
     * Gets query for [[Attributes0]].
     *
     * @return \yii\db\ActiveQuery|AttributesQuery
     */
    public function getAttributes0()
    {
        return $this->hasMany(Attributes::className(), ['id' => 'attributes_id'])->viaTable('autos_attributes', ['autos_id' => 'id']);
    }

    /**
     * Gets query for [[Images]].
     *
     * @return \yii\db\ActiveQuery|ImagesQuery
     */
    public function getImages()
    {
        return $this->hasMany(Images::className(), ['auto_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return AutosQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new AutosQuery(get_called_class());
    }
}
