<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "autos_attributes".
 *
 * @property int $autos_id
 * @property int $attributes_id
 * @property string|null $value
 *
 * @property Attributes $attributes0
 * @property Autos $autos
 */
class AutosAttributes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'autos_attributes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['autos_id', 'attributes_id'], 'required'],
            [['autos_id', 'attributes_id'], 'integer'],
            [['value'], 'string', 'max' => 255],
            [['autos_id', 'attributes_id'], 'unique', 'targetAttribute' => ['autos_id', 'attributes_id']],
            [['attributes_id'], 'exist', 'skipOnError' => true, 'targetClass' => Attributes::className(), 'targetAttribute' => ['attributes_id' => 'id']],
            [['autos_id'], 'exist', 'skipOnError' => true, 'targetClass' => Autos::className(), 'targetAttribute' => ['autos_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'autos_id' => Yii::t('backend/autos', 'Autos ID'),
            'attributes_id' => Yii::t('backend/autos', 'Attributes ID'),
            'value' => Yii::t('backend/autos', 'Value'),
        ];
    }

    /**
     * Gets query for [[Attributes0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAttributes0()
    {
        return $this->hasOne(Attributes::className(), ['id' => 'attributes_id']);
    }

    /**
     * Gets query for [[Autos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAutos()
    {
        return $this->hasOne(Autos::className(), ['id' => 'autos_id']);
    }
}
