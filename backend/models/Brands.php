<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "brands".
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $categories
 * @property string|null $img
 *
 * @property Autos[] $autos
 */
class Brands extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'brands';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'categories', 'img'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend/brands', 'ID'),
            'name' => Yii::t('backend/brands', 'Name'),
            'categories' => Yii::t('backend/brands', 'Categories'),
            'img' => Yii::t('backend/brands', 'Img'),
        ];
    }

    /**
     * Gets query for [[Autos]].
     *
     * @return \yii\db\ActiveQuery|AutosQuery
     */
    public function getAutos()
    {
        return $this->hasMany(Autos::className(), ['brand_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return BrandsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new BrandsQuery(get_called_class());
    }
}
