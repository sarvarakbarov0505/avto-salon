<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%images}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%autos}}`
 */
class m201101_061231_create_images_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%images}}', [
            'id' => $this->primaryKey(),
            'auto_id' => $this->integer(),
            'original_image' => $this->string(255),
            'image' => $this->string(255),
        ]);

        // creates index for column `auto_id`
        $this->createIndex(
            '{{%idx-images-auto_id}}',
            '{{%images}}',
            'auto_id'
        );

        // add foreign key for table `{{%autos}}`
        $this->addForeignKey(
            '{{%fk-images-auto_id}}',
            '{{%images}}',
            'auto_id',
            '{{%autos}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%autos}}`
        $this->dropForeignKey(
            '{{%fk-images-auto_id}}',
            '{{%images}}'
        );

        // drops index for column `auto_id`
        $this->dropIndex(
            '{{%idx-images-auto_id}}',
            '{{%images}}'
        );

        $this->dropTable('{{%images}}');
    }
}
