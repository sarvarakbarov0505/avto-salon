<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%autos}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%brands}}`
 * - `{{%categories}}`
 */
class m201101_053808_create_autos_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%autos}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'brand_id' => $this->integer(),
            'slug' => $this->string(255),
            'category_id' => $this->integer(),
        ]);

        // creates index for column `brand_id`
        $this->createIndex(
            '{{%idx-autos-brand_id}}',
            '{{%autos}}',
            'brand_id'
        );

        // add foreign key for table `{{%brands}}`
        $this->addForeignKey(
            '{{%fk-autos-brand_id}}',
            '{{%autos}}',
            'brand_id',
            '{{%brands}}',
            'id',
            'CASCADE'
        );

        // creates index for column `category_id`
        $this->createIndex(
            '{{%idx-autos-category_id}}',
            '{{%autos}}',
            'category_id'
        );

        // add foreign key for table `{{%categories}}`
        $this->addForeignKey(
            '{{%fk-autos-category_id}}',
            '{{%autos}}',
            'category_id',
            '{{%categories}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%brands}}`
        $this->dropForeignKey(
            '{{%fk-autos-brand_id}}',
            '{{%autos}}'
        );

        // drops index for column `brand_id`
        $this->dropIndex(
            '{{%idx-autos-brand_id}}',
            '{{%autos}}'
        );

        // drops foreign key for table `{{%categories}}`
        $this->dropForeignKey(
            '{{%fk-autos-category_id}}',
            '{{%autos}}'
        );

        // drops index for column `category_id`
        $this->dropIndex(
            '{{%idx-autos-category_id}}',
            '{{%autos}}'
        );

        $this->dropTable('{{%autos}}');
    }
}
