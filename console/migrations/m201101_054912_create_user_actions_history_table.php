<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%user_actions_history}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%user}}`
 */
class m201101_054912_create_user_actions_history_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%user_actions_history}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'model_name' => $this->string(255),
            'model_id' => $this->integer(),
            'craeted_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        // creates index for column `user_id`
        $this->createIndex(
            '{{%idx-user_actions_history-user_id}}',
            '{{%user_actions_history}}',
            'user_id'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-user_actions_history-user_id}}',
            '{{%user_actions_history}}',
            'user_id',
            '{{%user}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-user_actions_history-user_id}}',
            '{{%user_actions_history}}'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            '{{%idx-user_actions_history-user_id}}',
            '{{%user_actions_history}}'
        );

        $this->dropTable('{{%user_actions_history}}');
    }
}
