<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%autos_attributes}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%autos}}`
 * - `{{%attributes}}`
 */
class m201101_054052_create_junction_table_for_autos_and_attributes_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%autos_attributes}}', [
            'autos_id' => $this->integer(),
            'attributes_id' => $this->integer(),
            'value' => $this->string(255),
            'PRIMARY KEY(autos_id, attributes_id)',
        ]);

        // creates index for column `autos_id`
        $this->createIndex(
            '{{%idx-autos_attributes-autos_id}}',
            '{{%autos_attributes}}',
            'autos_id'
        );

        // add foreign key for table `{{%autos}}`
        $this->addForeignKey(
            '{{%fk-autos_attributes-autos_id}}',
            '{{%autos_attributes}}',
            'autos_id',
            '{{%autos}}',
            'id',
            'CASCADE'
        );

        // creates index for column `attributes_id`
        $this->createIndex(
            '{{%idx-autos_attributes-attributes_id}}',
            '{{%autos_attributes}}',
            'attributes_id'
        );

        // add foreign key for table `{{%attributes}}`
        $this->addForeignKey(
            '{{%fk-autos_attributes-attributes_id}}',
            '{{%autos_attributes}}',
            'attributes_id',
            '{{%attributes}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%autos}}`
        $this->dropForeignKey(
            '{{%fk-autos_attributes-autos_id}}',
            '{{%autos_attributes}}'
        );

        // drops index for column `autos_id`
        $this->dropIndex(
            '{{%idx-autos_attributes-autos_id}}',
            '{{%autos_attributes}}'
        );

        // drops foreign key for table `{{%attributes}}`
        $this->dropForeignKey(
            '{{%fk-autos_attributes-attributes_id}}',
            '{{%autos_attributes}}'
        );

        // drops index for column `attributes_id`
        $this->dropIndex(
            '{{%idx-autos_attributes-attributes_id}}',
            '{{%autos_attributes}}'
        );

        $this->dropTable('{{%autos_attributes}}');
    }
}
