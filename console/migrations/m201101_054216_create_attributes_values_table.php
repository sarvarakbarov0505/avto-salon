<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%attributes_values}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%attributes}}`
 */
class m201101_054216_create_attributes_values_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%attributes_values}}', [
            'id' => $this->primaryKey(),
            'attribute_id' => $this->integer(),
            'name' => $this->string(255),
        ]);

        // creates index for column `attribute_id`
        $this->createIndex(
            '{{%idx-attributes_values-attribute_id}}',
            '{{%attributes_values}}',
            'attribute_id'
        );

        // add foreign key for table `{{%attributes}}`
        $this->addForeignKey(
            '{{%fk-attributes_values-attribute_id}}',
            '{{%attributes_values}}',
            'attribute_id',
            '{{%attributes}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%attributes}}`
        $this->dropForeignKey(
            '{{%fk-attributes_values-attribute_id}}',
            '{{%attributes_values}}'
        );

        // drops index for column `attribute_id`
        $this->dropIndex(
            '{{%idx-attributes_values-attribute_id}}',
            '{{%attributes_values}}'
        );

        $this->dropTable('{{%attributes_values}}');
    }
}
