<?php
namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\db\Expression;
use yii\helpers\Html;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $verification_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 */
class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_INACTIVE = 9;
    const STATUS_ACTIVE = 10;

    const ROLE_ADMIN = 1;
    const ROLE_MODERATOR = 2;

    public $password;
    public $new_password;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fio','username'],'required'],
            [['password'], 'required', 'when' => function($model){return $model->isNewRecord;}],
            [['fio','username','password','new_password'],'string', 'max' => 255],

            [['username'],'unique'],
            [['status','role'],'integer'],
            ['status', 'default', 'value' => self::STATUS_INACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_INACTIVE, self::STATUS_DELETED]],

            ['role', 'default', 'value' => self::ROLE_MODERATOR],
            ['role', 'in', 'range' => [self::ROLE_MODERATOR, self::ROLE_ADMIN]],
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend/brands', 'ID'),
            'fio' => Yii::t('common/user', 'Fio'),
            'username' => Yii::t('common/user', 'Username'),
            'avatar' => Yii::t('common/user', 'Profil Picture'),
            'status' => Yii::t('common/user', 'Status'),
            'role' => Yii::t('common/user', 'Role'),
            'password' => Yii::t('common/user', 'Password'),
            'new password' => Yii::t('common/user', 'New Password'),
            'created_at' => Yii::t('common/user', 'Created At'),
            'updated_at' => Yii::t('common/user', 'Updated At'),
        ];
    }


    /**
    * @param bool $insert
    *
    * @return bool
    */
    public function beforeSave($insert)
    {
        if($this->isNewRecord){
            $this->generateAuthKey();
            $this->setPassword($this->password);
        }

        if(isset($this->new_password)){
            $this->setPassword($this->new_password);
        }

        return parent::beforeSave($insert);
    }


    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds user by verification email token
     *
     * @param string $token verify email token
     * @return static|null
     */
    public static function findByVerificationToken($token) {
        return static::findOne([
            'verification_token' => $token,
            'status' => self::STATUS_INACTIVE
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Generates new token for email verification
     */
    public function generateEmailVerificationToken()
    {
        $this->verification_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    public static function getStatusList()
    {
        return [
            self::STATUS_ACTIVE => Yii::t('coomon/user','Active'),
            self::STATUS_INACTIVE => Yii::t('coomon/user','Inactive'),
            self::STATUS_DELETED => Yii::t('coomon/user','Deleted'),
        ];
    }

    public static function getRoleList()
    {
        return [
            self::ROLE_MODERATOR => Yii::t('coomon/user','Moderator'),
            self::ROLE_ADMIN => Yii::t('coomon/user','Admin'),
        ];
    }

    public function getCreatAt()
    {
        return Yii::$app->formatter->asDate($this->created_at, 'HH:mm dd-MM-yyyy');
    }

    public function getUpdatedAt()
    {
        return Yii::$app->formatter->asDate($this->updated_at, 'HH:mm dd-MM-yyyy');
    }

    public function getImageAdress()
    {
        $dirname = 'users';
        if($this->avatar && file_exists("img". $dirname . "/" . $avatar)){
            return Html::img('@web/uploads/' . $dirname . '/' . $avatar, [
                'alt' => Yii::t('app','Profile Image'),
                'style' => "width:200px; margin:20px;",
                'id' => 'preview-image'
                ]);
        }else{
            return Html::img('@web/uploads/noimg.png', [
                'alt' => '',
                'style' => "width:200px; margin:20px;",
                'id' => 'preview-image'
                ]);
        }
    }
}
